-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 20 2020 г., 20:57
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `product`
--

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE `book` (
  `ID` int(10) UNSIGNED NOT NULL,
  `SKU` varchar(9) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `PRICE` smallint(7) NOT NULL,
  `WEIGHT` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`ID`, `SKU`, `NAME`, `PRICE`, `WEIGHT`) VALUES
(63, 'JVC200622', 'Harry Potter', 30, 1),
(66, 'JVC200122', 'Fahrenheit 451', 40, 2),
(67, 'JVC203245', 'Tom Sawyer', 30, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dvd-disc`
--

CREATE TABLE `dvd-disc` (
  `ID` int(10) UNSIGNED NOT NULL,
  `SKU` varchar(9) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `PRICE` smallint(7) NOT NULL,
  `SIZE` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dvd-disc`
--

INSERT INTO `dvd-disc` (`ID`, `SKU`, `NAME`, `PRICE`, `SIZE`) VALUES
(160, 'JVC200100', 'Rembo', 10, 500),
(159, 'JVC200665', 'God Father', 10, 300),
(158, 'JVC200122', 'Terminator', 5, 800);

-- --------------------------------------------------------

--
-- Структура таблицы `furniture`
--

CREATE TABLE `furniture` (
  `ID` int(10) UNSIGNED NOT NULL,
  `SKU` varchar(9) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `PRICE` smallint(7) NOT NULL,
  `HEIGHT` smallint(4) NOT NULL,
  `WIDTH` smallint(4) NOT NULL,
  `LENGTH` smallint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `furniture`
--

INSERT INTO `furniture` (`ID`, `SKU`, `NAME`, `PRICE`, `HEIGHT`, `WIDTH`, `LENGTH`) VALUES
(39, 'JVC20011', 'Enjoy', 500, 40, 180, 200);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- Индексы таблицы `dvd-disc`
--
ALTER TABLE `dvd-disc`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- Индексы таблицы `furniture`
--
ALTER TABLE `furniture`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `book`
--
ALTER TABLE `book`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT для таблицы `dvd-disc`
--
ALTER TABLE `dvd-disc`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT для таблицы `furniture`
--
ALTER TABLE `furniture`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
